import 'dart:html';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class location extends StatefulWidget {
  @override
  State<location> createState() => _locationState();
}

class _locationState extends State<location> {
  final Stream<QuerySnapshot> _locationStream = FirebaseFirestore.instance
      .collection('/companies/mWazXBjLxxvHnvTBXZq9/locations')
      .snapshots();
  CollectionReference location = FirebaseFirestore.instance
      .collection('/companies/mWazXBjLxxvHnvTBXZq9/locations');
  Future<void> delLocation(userId) {
    return location
        .doc(userId)
        .delete()
        .then((value) => print('Location Delete'))
        .catchError((error) => print('Failed to dalete Location: $error'));
  }

  // final Stream<QuerySnapshot> _userStream = FirebaseFirestore.instance
  //     .collection('/companies/mWazXBjLxxvHnvTBXZq9/locations')
  //     .snapshots();
  // CollectionReference users = FirebaseFirestore.instance
  //     .collection('/companies/mWazXBjLxxvHnvTBXZq9/locations');
  // Future<void> delUser(userId) {
  //   return users
  //       .doc(userId)
  //       .delete()
  //       .then((value) => print('User Delete'))
  //       .catchError((error) => print('Failed to dalete user: $error'));
  // }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: _locationStream,
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text('Something went wrong');
        }
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Text('Loading..');
        }
        return ListView(
          children: snapshot.data!.docs.map((DocumentSnapshot document) {
            Map<String, dynamic> data =
                document.data()! as Map<String, dynamic>;

            return ListTile(
              title: Text(data['location']),
              subtitle: Text(data['opening_time']),
              trailing: IconButton(
                icon: Icon(Icons.delete),
                onPressed: () async {
                  await delLocation(document.id);
                },
              ),
            );
          }).toList(),
        );
      },
    );
  }
}
